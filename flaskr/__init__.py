from flask import Flask
from flask_restful import Resource, Api
from jsonschema import validate
from jsonschema.exceptions import SchemaError, ValidationError
from werkzeug.exceptions import InternalServerError

from flaskr.resources.demo import Content, EmployeeList
from flaskr.utils import load_json_schema, load_json_file


def create_app():
    application = Flask(__name__)
    api = Api(application)

    load_config()

    class HealthCheck(Resource):
        def get(self):
            return {'status': 'OK'}

    api.add_resource(HealthCheck, '/', '/health')

    # Routes
    api.add_resource(EmployeeList, '/employees')
    api.add_resource(Content, '/content/<type>')

    return application


CONFIG_SCHEMA_NAME = 'config-schema.json'


def load_config():
    try:
        config = load_json_file('samples/config-app.json')
        schema = load_json_schema(CONFIG_SCHEMA_NAME)
    except Exception:
        raise InternalServerError

    try:
        validate(config, schema)
    except SchemaError:
        # Log error about schema definition syntax error
        raise InternalServerError
    except ValidationError:
        # Log configuration file not valid
        raise InternalServerError

