from flask import request, make_response
from flask_restful import Resource
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from werkzeug.exceptions import BadRequest

from flaskr.utils import load_json_schema, load_json_file

CONTENT_TYPE_FILES = 'files'
CONTENT_TYPE_MULTIMEDIA = 'multimedia'

CONTENT_TYPE = {
    CONTENT_TYPE_FILES: 'samples/content_type_files.json',
    CONTENT_TYPE_MULTIMEDIA: 'samples/content_type_multimedia.json'
}

CONTENT_TYPE_WRONG = {
    CONTENT_TYPE_FILES: 'samples/content_type_multimedia.json',
    CONTENT_TYPE_MULTIMEDIA: 'samples/content_type_files.json'
}

EMPLOYEE_SCHEMA_NAME = 'employee-schema.json'


class Content(Resource):
    def get(self, type):
        if type in CONTENT_TYPE.keys():
            content = load_json_file(CONTENT_TYPE[type])
        else:
            raise BadRequest('Specified type does not exist')

        return content


class EmployeeList(Resource):

    def __init__(self):
        self.schema = load_json_schema(EMPLOYEE_SCHEMA_NAME)

    def post(self):
        req = request.get_json()

        try:
            validate(req, self.schema)
        except ValidationError:
            raise BadRequest('Request body is invalid!')

        # TODO: Store user in DB

        return make_response({'Success': 'New employee added'}, 200)
