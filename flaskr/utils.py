import json
import os

from werkzeug.exceptions import InternalServerError


def load_json_schema(schema_name: str) -> dict:
    return load_json_file('schemas/{}'.format(schema_name))


def load_json_file(file_path: str) -> dict:
    file_dir = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        file_path
    )

    try:
        with open(file_dir) as fp:
            content = json.load(fp)
    except Exception:
        raise InternalServerError

    return content
