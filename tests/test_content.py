from jsonschema import validate

from flaskr.utils import load_json_schema

FILES_SCHEMA_NAME = 'content-type-files-schema.json'
MULTIMEDIA_SCHEMA_NAME = 'content-type-multimedia-schema.json'


def test_get_files_returns_file_content(client):
    response = client.get("/content/files")

    schema = load_json_schema(FILES_SCHEMA_NAME)

    validate(response.json, schema)


def test_get_multimedia_returns_multimedia_content(client):
    response = client.get("/content/multimedia")

    schema = load_json_schema(MULTIMEDIA_SCHEMA_NAME)

    validate(response.json, schema)
